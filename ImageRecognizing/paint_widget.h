#pragma once
#include <QGraphicsScene>

class GraphicsScene : public QGraphicsScene
{
	Q_OBJECT;
public:
	GraphicsScene(QObject* parent = nullptr);
	~GraphicsScene() = default;

protected:
	virtual void mousePressEvent(QGraphicsSceneMouseEvent* event) override;
	virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* event) override;
private:
	QPointF m_previous_point;


};
