﻿find_package(Qt5 COMPONENTS REQUIRED Core Gui Widgets OpenGL)

set(HEADERS
	main_window.h
	paint_widget.h
)

set(SOURCES
	main.cpp
	main_window.cpp
	paint_widget.cpp
)

source_group(headers FILES ${HEADERS})
source_group(src FILES ${SOURCES})

add_executable (ImageRecognizing ${HEADERS} ${SOURCES} ${MOC_SOURCES})

target_link_libraries(ImageRecognizing
	PUBLIC
	Perceptron
	Qt5::Core
	Qt5::Gui
	Qt5::Widgets
)

install(TARGETS ImageRecognizing)
install(FILES $<TARGET_FILE:Qt5::Core> $<TARGET_FILE:Qt5::Gui> $<TARGET_FILE:Qt5::Widgets> DESTINATION ${CMAKE_INSTALL_PREFIX}/bin)

if(WIN32)
	add_custom_command(TARGET ${PROJECT_NAME} POST_BUILD
		COMMAND ${CMAKE_COMMAND} -E copy_if_different $<TARGET_FILE:Qt5::Core> $<TARGET_FILE_DIR:${PROJECT_NAME}>
		COMMAND ${CMAKE_COMMAND} -E copy_if_different $<TARGET_FILE:Qt5::Gui> $<TARGET_FILE_DIR:${PROJECT_NAME}>
		COMMAND ${CMAKE_COMMAND} -E copy_if_different $<TARGET_FILE:Qt5::Widgets> $<TARGET_FILE_DIR:${PROJECT_NAME}> )
endif(WIN32)

