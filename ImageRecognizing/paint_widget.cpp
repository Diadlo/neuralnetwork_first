#include "paint_widget.h"
#include <QGraphicsSceneMouseEvent>

GraphicsScene::GraphicsScene(QObject* parent)
	: QGraphicsScene(parent)
{
}

void GraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    addEllipse(event->scenePos().x() - 5, event->scenePos().y() - 5, 10, 10, QPen(Qt::NoPen), QBrush(Qt::black));
	m_previous_point = event->scenePos();
}

void GraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
	addLine(m_previous_point.x(), m_previous_point.y(),
		event->scenePos().x(), event->scenePos().y(),
        QPen(Qt::black, 10, Qt::SolidLine, Qt::RoundCap));
	m_previous_point = event->scenePos();
}
