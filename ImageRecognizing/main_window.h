#pragma once
#include "paint_widget.h"
#include "Perceptron.h"
#include <QWidget>
#include <QGraphicsView>
#include <QTimer>
#include <memory>

class MainWindow : public QWidget
{
    Q_OBJECT
public:
    MainWindow(QWidget* parent = nullptr);

protected:
	virtual void resizeEvent(QResizeEvent* event) override;

private:
    void recognizeImage();

	QGraphicsView* m_graphics_view = nullptr;
	GraphicsScene* m_graphics_scene = nullptr;
	QTimer* m_timer = nullptr;

    std::unique_ptr<Perceptron> m_perceptron;
};
