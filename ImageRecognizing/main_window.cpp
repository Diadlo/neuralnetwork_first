#include "main_window.h"
#include <QVBoxLayout>
#include <QPushButton>
#include <QGraphicsView>
#include <QMessageBox>
#include <iostream>

MainWindow::MainWindow(QWidget* parent)
	: QWidget(parent)
{
	setFixedSize(350, 400);

	m_graphics_scene = new GraphicsScene(this);
    m_graphics_scene->setBackgroundBrush(QBrush(Qt::white));

    m_graphics_view = new QGraphicsView();
	m_graphics_view->setScene(m_graphics_scene);
	m_graphics_view->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
	m_graphics_view->setVerticalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
	m_graphics_view->setHorizontalScrollBarPolicy(Qt::ScrollBarPolicy::ScrollBarAlwaysOff);
	m_graphics_view->setAlignment(Qt::AlignCenter);

    auto layout = new QVBoxLayout();
    auto recognizeBtn = new QPushButton("Recognize");
    auto clearBtn = new QPushButton("Clear");
    connect(recognizeBtn, &QPushButton::clicked, this, &MainWindow::recognizeImage);
    connect(clearBtn, &QPushButton::clicked, this, [=] {
		m_graphics_scene->clear();
	});

    auto buttonsLayout = new QHBoxLayout();
    buttonsLayout->addWidget(recognizeBtn);
    buttonsLayout->addWidget(clearBtn);
    buttonsLayout->setAlignment(Qt::AlignRight);

	layout->addWidget(m_graphics_view, 0, Qt::AlignCenter);
    layout->addLayout(buttonsLayout);
	setLayout(layout);
	const auto width = m_graphics_view->size().width();
	const auto height = m_graphics_view->size().height();
	m_graphics_scene->setSceneRect(0, 0, width, height);

    const auto size = static_cast<size_t>(width * height);
    m_perceptron = std::make_unique<Perceptron>(size, 1);
}

void MainWindow::resizeEvent(QResizeEvent* event)
{
	QWidget::resizeEvent(event);
}

void MainWindow::recognizeImage()
{
    const auto rect = m_graphics_scene->sceneRect();
    const auto width = static_cast<int>(rect.width());
    const auto height = static_cast<int>(rect.height());
    auto image = QImage(width, height, QImage::Format_RGB32);
    QPainter painter(&image);
    m_graphics_scene->render(&painter);

	std::vector<int> values;
    values.reserve(static_cast<size_t>(width * height));
    for (int y = 0; y < image.height(); y++) {
		for (int x = 0; x < image.width(); x++) {
            QRgb pixel = image.pixel(x, y);
            const int gray = qGray(pixel);
            const int value = gray > (255 / 2) ? 0 : 1;
			values.push_back(value);
		}
    }

    Perceptron::Result result = m_perceptron->recognize(values);
    const QString answer = result == Perceptron::Result::First ? "Ф" : "К";

	auto button = QMessageBox::question(nullptr, "Answer", answer);
    if (button == QMessageBox::No) {
        m_perceptron->incorrect(values, result);
	}
}

