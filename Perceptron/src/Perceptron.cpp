#include "Perceptron.h"
#include <cstdlib>
#include <iostream>

namespace  {
Perceptron::Result anotherResult(Perceptron::Result res)
{
    if (res == Perceptron::Result::First)
        return Perceptron::Result::Second;
    else
        return Perceptron::Result::First;
}
}

Perceptron::Perceptron(size_t size, double velocity)
    : m_v{velocity}
{
    m_w.resize(size);
    for (size_t i = 0; i < size; i++) {
        m_w[i] = (rand() % 7 - 3) * 0.1;
    }
}

Perceptron::Result Perceptron::recognize(const std::vector<int>& x)
{
    double result = 0;
    for (size_t i = 0; i < x.size(); i++) {
        result += m_w[i] * x[i];
    }

    if (result < 0) {
        return Result::First;
    } else {
        return Result::Second;
    }
}

void Perceptron::incorrect(const std::vector<int>& x, Perceptron::Result incorrect)
{
    const auto correct = anotherResult(incorrect);
    const int correct_val = correct   == Perceptron::Result::First ? 0 : 1;
    const int actual_val  = incorrect == Perceptron::Result::First ? 0 : 1;
    const double delta = correct_val - actual_val;
    for (size_t i = 0; i < x.size(); i++) {
        m_w[i] = m_w[i] + m_v * delta * x[i];
    }
}
