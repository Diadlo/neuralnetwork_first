#pragma once

#include <vector>

class Perceptron
{
public:
    enum class Result
    {
        First,
        Second
    };

    Perceptron(size_t size, double velocity);

    Result recognize(const std::vector<int>& x);
    void incorrect(const std::vector<int>& x, Perceptron::Result incorrect);

private:
    std::vector<double> m_w;
    double m_v;
};
